const { ApolloServer, gql } = require('apollo-server')

const chemicalType1 = require('./database/chemical_type_1.json')
const chemicalType2 = require('./database/chemical_type_2.json')

const {
    chemicalTotalDocuments,
    chemicalsList,
    chemicalsDictionary,
    patentsDictionary,
    relatedChemicals,
    relatedPatents,
    patentDocuments,
} = require('./helpers')

// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = gql`
    type Patent {
        patentNo: String
        patentTitle: String
    }

    type RelatedChemical {
        name: String
        partial: Int
    }

    type RelatedChemicalsByType {
        type1: [RelatedChemical]
        type2: [RelatedChemical]
    }

    type RelatedPatentsByType {
        type1: [Patent]
        type2: [Patent]
    }

    # This "Chemical" type defines the queryable fields for every Chemical in our data source.
    type Chemical {
        name: String
        total: Int
    }

    type Document {
        patentNo: String
        patentTitle: String
    }

    # The "Query" type is special: it lists all of the available queries that
    # clients can execute, along with the return type for each.
    type Query {
        name(name: String!): String
        total(name: String!): Int
        relatedChemicals(name: String!): RelatedChemicalsByType
        relatedPatents(name: String!): RelatedPatentsByType
        chemicals(name: String): [String]
        patents: [Document]
    }
`

const chemicalNames = chemicalsList(chemicalType1, chemicalType2)
const chemicals = chemicalsDictionary(chemicalType1, chemicalType2)
const patentsById = patentsDictionary(chemicalType1, chemicalType2)
const patents = patentDocuments(chemicalType1, chemicalType2)

// Resolvers define the technique for fetching the types defined in the
// schema. This resolver retrieves books from the "books" array above.
const resolvers = {
    Query: {
        name: (parent, args) => args.name.toLowerCase(),
        total: (parent, args) => chemicalTotalDocuments(args.name, chemicals),
        relatedChemicals: (parent, args) => relatedChemicals(args.name, chemicals, patentsById),
        relatedPatents: (parent, args) => chemicals[args.name.toLowerCase()],
        chemicals: (parent, args) => {
            if (args.name) {
                return chemicalNames.filter(chemical => chemical.startsWith(args.name))
            } else {
                return chemicalNames
            }
        },
        patents: () => patents,
    },
}

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({ typeDefs, resolvers })

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`)
})
