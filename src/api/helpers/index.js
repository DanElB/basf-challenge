const chemicalsList = (...dataTypes) => Array.from(new Set(dataTypes.flat().map(data => data.name.toLowerCase()))).sort()

const uniqueArray = (oldArray, field) =>
    Array.from(new Set(oldArray.map(e => e[field].toLowerCase())))
        .sort()
        .map(id => oldArray.find(e => e[field].toLowerCase() === id))

const chemicalTotalDocuments = (name, chemicals) => {
    const chemical = chemicals[name.toLowerCase()]
    const totalType1 = chemical.type1 && chemical.type1.length ? chemical.type1.length : 0
    const totalType2 = chemical.type2 && chemical.type2.length ? chemical.type2.length : 0
    return totalType1 + totalType2
}

const chemicalsDictionary = (...dataTypes) => {
    const newDictionary = {}

    dataTypes.forEach((type, i) => {
        type.forEach(row => {
            const chemical = row.name.toLowerCase()

            if (newDictionary[chemical]) {
                if (newDictionary[chemical][`type${i + 1}`]) {
                    const newEntry = [...newDictionary[chemical][`type${i + 1}`], { patentNo: row.patentNo, patentTitle: row.patentTitle }]
                    newDictionary[chemical][`type${i + 1}`] = newEntry
                } else {
                    newDictionary[chemical] = {
                        ...newDictionary[chemical],
                        [`type${i + 1}`]: [{ patentNo: row.patentNo, patentTitle: row.patentTitle }],
                    }
                }
            } else {
                newDictionary[chemical] = { [`type${i + 1}`]: [{ patentNo: row.patentNo, patentTitle: row.patentTitle }] }
            }
        })
    })

    return newDictionary
}

const patentsDictionary = (...dataTypes) => {
    const newDictionary = {}

    dataTypes.forEach((type, i) => {
        type.forEach(row => {
            const chemical = row.name.toLowerCase()

            if (newDictionary[row.patentNo]) {
                if (newDictionary[row.patentNo][`type${i + 1}`]) {
                    newDictionary[row.patentNo][`type${i + 1}`] = [...newDictionary[row.patentNo][`type${i + 1}`], chemical]
                } else {
                    newDictionary[row.patentNo][`type${i + 1}`] = [chemical]
                }
            } else {
                newDictionary[row.patentNo] = { id: row.patentNo, [`type${i + 1}`]: [chemical] }
            }
        })
    })

    return newDictionary
}

const relatedChemicals = (cName, chemicals, patents) => {
    if (cName) {
        const name = cName.toLowerCase()
        let info = {}
        const chemical = chemicals[name]

        Object.keys(chemical).forEach(type => {
            const typeResult = []
            if (chemical[type] && chemical[type].length) {
                chemical[type].forEach(p => {
                    const patent = patents[p.patentNo]
                    const typeChemicals = patent[type]

                    if (typeChemicals) {
                        typeChemicals.forEach(tChemicalName => {
                            const partial = chemicals[tChemicalName][type].length || 0
                            typeResult.push({ name: tChemicalName, partial })
                        })
                    }
                })
            }
            info = { ...info, [type]: uniqueArray(typeResult, 'name') }
        })
        return info
    }
}

const patentDocuments = (...dataTypes) => {
    return uniqueArray(
        dataTypes
            .map(type =>
                uniqueArray(
                    type.map(row => ({ patentNo: row.patentNo.toLowerCase(), patentTitle: row.patentTitle })),
                    'patentNo'
                )
            )
            .flat(),
        'patentNo'
    ).map(row => ({ patentNo: row.patentNo.toUpperCase(), patentTitle: row.patentTitle }))
}

exports.chemicalTotalDocuments = chemicalTotalDocuments
exports.chemicalsList = chemicalsList
exports.chemicalsDictionary = chemicalsDictionary
exports.patentsDictionary = patentsDictionary
exports.relatedChemicals = relatedChemicals
exports.patentDocuments = patentDocuments
