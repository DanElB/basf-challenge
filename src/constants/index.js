export const LOCAL_USER_THEME_KEY = 'user-theme'
export const COLOR_MODE_CSS_VAR = '--color-mode'

export const GOOGLE_PATENT_URL = 'https://patents.google.com/patent'
export const GOOGLE_CLIENT_ID = '465915104186-7beasvjmoricgcnunpg896fihupp3f9t.apps.googleusercontent.com'
