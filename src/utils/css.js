const getStyles = () => getComputedStyle(document.documentElement)

export const getCSSCustomProp = propKey => {
    let response = getStyles().getPropertyValue(propKey)

    if (response.length) {
        response = response.replace(/"/g, '').trim()
    }

    return response
}
