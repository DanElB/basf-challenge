import { LOCAL_USER_THEME_KEY, COLOR_MODE_CSS_VAR } from '../constants'
import { getCSSCustomProp } from '.'

export const applyTheme = theme => {
    let currentTheme = theme || localStorage.getItem(LOCAL_USER_THEME_KEY)

    if (currentTheme) {
        document.documentElement.setAttribute(`data-${LOCAL_USER_THEME_KEY}`, currentTheme)
    }
}

export const toggleTheme = () => {
    let currentTheme = localStorage.getItem(LOCAL_USER_THEME_KEY)

    switch (currentTheme) {
        case 'light':
            currentTheme = 'dark'
            break
        case 'dark':
            currentTheme = 'light'
            break
        default:
            currentTheme = getCSSCustomProp(COLOR_MODE_CSS_VAR) === 'dark' ? 'light' : 'dark'
            break
    }

    localStorage.setItem(LOCAL_USER_THEME_KEY, currentTheme)

    return currentTheme
}
