import React, { useState } from 'react'

import { User } from './types'

import { UserContext } from './context/User'

import styles from './App.module.css'
import './styles/_resets.css'
import './styles/_colors.css'
import './styles/_theme.css'

import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary'
import Drawer from './components/Drawer/Drawer'
import SearchScene from './components/SearchScene/SearchScene'
import DocumentScene from './components/DocumentScene/DocumentScene'

import { Grid, Row, Col } from 'react-flexbox-grid'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'

const App: React.FC<{}> = () => {
    const [user, setUser] = useState<User>()

    return (
        <ErrorBoundary>
            <Router>
                <main>
                    <UserContext.Provider value={{ user, setUser }}>
                        <Grid fluid>
                            <Row className={styles.mainRow}>
                                <Col>
                                    <Drawer />
                                </Col>
                                <Col xs className={styles.mainContent}>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <Switch>
                                                    <Route exact path="/">
                                                        <Redirect to="/search" />
                                                    </Route>
                                                    <Route path="/search">
                                                        <SearchScene />
                                                    </Route>
                                                    <Route path="/documents">
                                                        <DocumentScene />
                                                    </Route>
                                                    <Route>
                                                        <Redirect to="/search" />
                                                    </Route>
                                                </Switch>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </Col>
                            </Row>
                        </Grid>
                    </UserContext.Provider>
                </main>
            </Router>
        </ErrorBoundary>
    )
}

export default App
