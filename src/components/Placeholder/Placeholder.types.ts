import CSS from 'csstype'

export type PlaceholderProps = {
    style?: CSS.Properties
}
