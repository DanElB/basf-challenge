import React from 'react'

import { PlaceholderProps } from './Placeholder.types'
import styles from './Placeholder.module.css'

const Placeholder: React.FC<PlaceholderProps> = ({ style }) => <div className={styles.box} style={style}></div>

export default Placeholder
