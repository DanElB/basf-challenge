import React, { useState, useEffect } from 'react'

import { TableProps, Row } from './Table.types'
import styles from './Table.module.css'

import Input from '../Input/Input'

import { ReactComponent as EyeIcon } from '../../assets/icons/eye.svg'

const Table: React.FC<TableProps> = ({ headers, rows, hasActions = false, hasIndex = false, onRowClick }) => {
    const [renderRows, setRenderRows] = useState(rows)

    useEffect(() => {
        setRenderRows(rows)
    }, [rows])

    const handleRowClick = (row: Row) => () => {
        if (onRowClick) {
            onRowClick(row)
        }
    }

    const handleFilter = (event: any) => {
        const newRows = renderRows?.filter(
            row =>
                row.cells[0].value?.toString().toLowerCase().includes(event.target.value.toLowerCase()) ||
                row.cells[1].value?.toString().toLowerCase().includes(event.target.value.toLowerCase())
        )
        setRenderRows(newRows)
        if (!event.target.value) {
            setRenderRows(rows)
        }
    }

    return (
        <div className={styles.container}>
            <Input type="text" onChange={handleFilter} placeholder="Search" />
            <table className={styles.table}>
                <div className={styles.scrollContainer}>
                    <thead>
                        <tr>
                            {hasIndex && <th className={styles.header}></th>}
                            {headers?.map(header => (
                                <th className={styles.header} key={header?.name}>
                                    {header.name}
                                </th>
                            ))}
                            {hasActions && <th className={styles.header}>Actions</th>}
                        </tr>
                    </thead>
                    <tbody className={styles.tableBody}>
                        {renderRows?.map((row, index) => (
                            <tr className={styles.row} onClick={handleRowClick(row)} key={`tbody_row_${index}`}>
                                {hasIndex && <td className={styles.cell}>{index}</td>}
                                {row?.cells.map((cell, i) => (
                                    <td className={styles.cell} key={`cell_${i}`}>
                                        {cell.value}
                                    </td>
                                ))}
                                {hasActions && (
                                    <td className={styles.cell}>
                                        {row?.link && (
                                            <a href={row.link} target="_blank">
                                                <EyeIcon className={styles.link} />
                                            </a>
                                        )}
                                    </td>
                                )}
                            </tr>
                        ))}
                    </tbody>
                </div>
            </table>
        </div>
    )
}

export default Table
