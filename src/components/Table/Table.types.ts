export type Header = {
    name: string
    sortFunction?: 'alphabetically' | 'numeric' | undefined
}

export type Cell = {
    column: string
    value?: string | number | undefined
}

export type Row = {
    cells: Cell[]
    pdf?: string
    link?: string
}

export type TableProps = {
    headers?: Header[]
    rows?: Row[]
    hasActions?: boolean
    hasIndex?: boolean
    onRowClick?: Function
}
