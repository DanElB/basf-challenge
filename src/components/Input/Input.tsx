import React from 'react'

import { InputProps } from './Input.types'
import styles from './Input.module.css'

const Input: React.FC<InputProps> = ({ ...props }) => {
    return <input {...props} className={styles.input} />
}

export default Input
