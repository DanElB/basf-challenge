import React from 'react'

import { User } from '../../types'
import { useUser } from '../../context/User'
import { GOOGLE_CLIENT_ID } from '../../constants'

import styles from './Login.module.css'

import { GoogleLogin } from 'react-google-login'

const Login = ({}) => {
    const userCtx = useUser()

    const handleGoogleSuccess = (response: any) => {
        const loggedUser: User = {
            name: response?.profileObj?.name,
            email: response?.profileObj?.email,
            image: response?.profileObj?.imageUrl,
            isLogged: true,
        }
        userCtx.setUser(loggedUser)
    }

    const handleGoogleFailure = (response: any) => {
        throw new Error(response)
    }

    if (!userCtx.user?.isLogged) {
        return (
            <div className={styles.login}>
                <GoogleLogin
                    clientId={GOOGLE_CLIENT_ID}
                    buttonText="Login with Google"
                    onSuccess={handleGoogleSuccess}
                    onFailure={handleGoogleFailure}
                    cookiePolicy={'single_host_origin'}
                />
            </div>
        )
    }
    return null
}

export default Login
