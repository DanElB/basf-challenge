import React from 'react'

import { CardProps } from './Card.types'

import styles from './Card.module.css'

const Card: React.FC<CardProps> = ({ title, children }) => (
    <section className={styles.card}>
        {title && <p className={styles.title}>{title}</p>}
        {children}
    </section>
)

export default Card
