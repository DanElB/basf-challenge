import React from 'react'

import styles from './Loader.module.css'

const Loader = ({}) => (
    <div className={styles.atomSpinner}>
        <div className={styles.spinnerInner}>
            <div className={styles.spinnerLine}></div>
            <div className={styles.spinnerLine}></div>
            <div className={styles.spinnerLine}></div>
            <div className={styles.spinnerCircle}>&#9679;</div>
        </div>
    </div>
)

export default Loader
