import React from 'react'

import styles from './SearchScene.module.css'

import { useUser } from '../../context/User'

import Loader from '../Loader/Loader'
import QueryInput from '../QueryInput/QueryInput'
import ChemicalInfo from './components/ChemicalInfo'
import { ReactComponent as SearchIcon } from '../../assets/icons/search.svg'

import { useLazyQuery } from '@apollo/client'
import { GET_CHEMICAL } from '../../endpoints'

import { Row, Col } from 'react-flexbox-grid'

const SearchScene = ({}) => {
    const [getChemical, { loading, data }] = useLazyQuery(GET_CHEMICAL)
    const userCtx = useUser()

    const handleInputSelect: (chemicalQuery: string) => void = chemicalQuery => {
        getChemical({ variables: { name: chemicalQuery } })
    }

    return (
        <div>
            <Row center="xs">
                <Col xs={12}>
                    <h1 className={styles.welcome}>Welcome {userCtx?.user?.name}</h1>
                </Col>
                <Col xs={12}>
                    <h2 className={styles.title}>What chemical would you like to look for?</h2>
                </Col>
                <Col xs={12}>
                    <div className={styles.search}>
                        <QueryInput onSelect={handleInputSelect} className={styles.searchField} />
                        <SearchIcon className={styles.searchIcon} />
                    </div>
                </Col>
                <Row center="xs">
                    <Col xs={12}>{loading ? <Loader /> : <ChemicalInfo info={data} />}</Col>
                </Row>
            </Row>
        </div>
    )
}

export default SearchScene
