import React, { useState, useEffect } from 'react'

import { ChemicalInfoProps } from './ChemicalInfo.types'
import { RelatedPatent, RelatedChemicals, RelatedChemical } from '../../../types'
import { Header, Row } from '../../Table/Table.types'

import styles from './ChemicalInfo.module.css'

import Card from '../../Card/Card'
import Table from '../../Table/Table'

import { Row as RowComponent, Col } from 'react-flexbox-grid'
import { useHistory } from 'react-router-dom'

const tableHeaders: Header[] = [
    { name: 'name', sortFunction: 'alphabetically' },
    { name: 'appearances', sortFunction: 'numeric' },
]

const ChemicalInfo: React.FC<ChemicalInfoProps> = ({ info }) => {
    const [rowsType1, setRowsType1] = useState<Row[]>([])
    const [rowsType2, setRowsType2] = useState<Row[]>([])

    const history = useHistory()

    const setElements = (
        patentsArr: RelatedPatent[] | undefined,
        relatedChemicalArr: RelatedChemical[] | undefined | null,
        setFn: Function
    ): void => {
        let firstRow = {}
        let relatedChemicalRows: Row[] = []
        if (patentsArr?.length) {
            firstRow = {
                cells: [
                    { column: 'name', value: info?.name },
                    { column: 'appearances', value: patentsArr.length },
                ],
            }
        }
        if (relatedChemicalArr?.length) {
            relatedChemicalRows = relatedChemicalArr
                .filter(chemical => chemical?.name !== info?.name)
                .map((relatedChemical: RelatedChemical) => ({
                    cells: [
                        { column: 'name', value: relatedChemical?.name },
                        { column: 'appearances', value: relatedChemical?.partial },
                    ],
                }))
        }

        if (Object.entries(firstRow).length) {
            setFn([firstRow, ...relatedChemicalRows])
        } else {
            setFn([...relatedChemicalRows])
        }
    }

    useEffect(() => {
        setElements(info?.relatedPatents?.type1, info?.relatedChemicals?.type1, setRowsType1)
        setElements(info?.relatedPatents?.type2, info?.relatedChemicals?.type2, setRowsType2)
    }, [info])

    const handleRowClick = (row: Row) => {
        const name = row?.cells[0]?.value as string
        history.push(`documents?chemical=${name}`)
    }

    const NoResults = () => <p style={{ width: 300, lineHeight: 1.5 }}>This chemical does not have information in this data set</p>

    if (info) {
        return (
            <section>
                <RowComponent center="xs">
                    <Col xs={7}>
                        {!!info?.total && (
                            <p className={styles.total}>
                                Found <span className={styles.accent}>{info.total}</span> documents for{' '}
                                <span className={styles.accent}>{info?.name}</span>
                            </p>
                        )}
                    </Col>
                </RowComponent>
                <RowComponent center="xs">
                    <Col sm={12} md={6}>
                        <Card title="Chemical Type 1">
                            {rowsType1?.length ? <Table headers={tableHeaders} rows={rowsType1} onRowClick={handleRowClick} /> : <NoResults />}
                        </Card>
                    </Col>
                    <Col sm={12} md={6}>
                        <Card title="Chemical Type 2">
                            {rowsType2?.length ? <Table headers={tableHeaders} rows={rowsType2} onRowClick={handleRowClick} /> : <NoResults />}
                        </Card>
                    </Col>
                </RowComponent>
            </section>
        )
    }

    return null
}

export default ChemicalInfo
