import { Chemical } from '../../../types'

export type ChemicalInfoProps = {
    info?: Chemical
}
