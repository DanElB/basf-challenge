import React from 'react'

import styles from './Link.module.css'

import { NavLink, NavLinkProps } from 'react-router-dom'

const Link: React.FC<NavLinkProps> = ({ to, children }) => (
    <NavLink to={to} className={styles.normal} activeClassName={styles.active}>
        {children}
    </NavLink>
)

export default Link
