import React, { useState, useEffect } from 'react'

import { QueryInputProps } from './QueryInput.types'
import styles from './QueryInput.module.css'

import Autocomplete from 'react-autocomplete'
import Input from '../Input/Input'
import classNames from 'classnames'
import { useQuery } from '@apollo/client'

import { GET_CHEMICALS } from '../../endpoints'

const QueryInput: React.FC<QueryInputProps> = ({ onSelect, className }) => {
    const [value, setValue] = useState<string | undefined>()

    const { data } = useQuery(GET_CHEMICALS)

    const handleInputChange: React.ChangeEventHandler<HTMLInputElement> = event => {
        setValue(event.target.value)
    }

    const handleOnSelect = (val: string) => {
        setValue(val)
    }

    useEffect(() => {
        if (value) {
            onSelect(value)
        }
    }, [value])

    const renderChemicals: (state: any, val: string) => boolean = (state, val) => state.toLowerCase().indexOf(val.toLowerCase()) !== -1

    return (
        <div className={className}>
            <Autocomplete
                value={value}
                items={data?.chemicals}
                inputProps={{ className: styles.input, placeholder: 'Search' }}
                getItemValue={(item: string) => item}
                shouldItemRender={renderChemicals}
                renderMenu={(item: React.ReactNode[]) => <div className={styles.dropdown}>{item}</div>}
                renderItem={(item: string, isHighlighted: boolean) => (
                    <div className={classNames(styles.item, { [styles.selected]: isHighlighted })} key={item}>
                        {item}
                    </div>
                )}
                onChange={handleInputChange}
                onSelect={handleOnSelect}
            />
        </div>
    )
}

export default QueryInput
