export type QueryInputProps = {
    onSelect: Function
    className?: string
}
