import React, { useEffect, useState } from 'react'

import { Header, Row } from '../Table/Table.types'

import styles from './DocumentScene.module.css'

import { GOOGLE_PATENT_URL } from '../../constants'
import { arrayUniqueElements } from '../../utils'

import Card from '../Card/Card'
import Table from '../Table/Table'
import Loader from '../Loader/Loader'

import { useLocation } from 'react-router-dom'
import qs from 'qs'

import { useLazyQuery } from '@apollo/client'
import { GET_CHEMICAL, GET_PATENTS } from '../../endpoints'

const tableHeaders: Header[] = [{ name: 'Document ID' }, { name: 'Document Title' }]

const DocumentScene = ({}) => {
    const [docRows, setDocRows] = useState<Row[]>([])

    const [getChemical, { loading, error, data }] = useLazyQuery(GET_CHEMICAL)
    const [getPatents, { loading: loadingPatents, error: errorPatents, data: dataPatents }] = useLazyQuery(GET_PATENTS)

    const location = useLocation()

    useEffect(() => {
        if (data) {
            const docs: Row[] = arrayUniqueElements([...(data?.relatedPatents?.type1 || []), ...(data?.relatedPatents?.type2 || [])]).map(patent => ({
                cells: [
                    { column: 'Document ID', value: patent.patentNo },
                    { column: 'Document Title', value: patent.patentTitle },
                ],
                link: `${GOOGLE_PATENT_URL}/${patent.patentNo}`,
            }))
            setDocRows(docs)
        }
    }, [data])

    useEffect(() => {
        if (dataPatents) {
            const docs: Row[] = dataPatents?.patents.map((patent: { patentNo: string; patentTitle: string }) => ({
                cells: [
                    { column: 'Document ID', value: patent.patentNo },
                    { column: 'Document Title', value: patent.patentTitle },
                ],
                link: `${GOOGLE_PATENT_URL}/${patent.patentNo}`,
            }))
            setDocRows(docs)
        }
    }, [dataPatents])

    useEffect(() => {
        if (location?.search) {
            const { chemical } = qs.parse(location.search, { ignoreQueryPrefix: true })
            getChemical({ variables: { name: chemical } })
        } else {
            getPatents()
        }
    }, [location])

    return (
        <div>
            <h2 className={styles.title}>Documents</h2>
            <Card title={`Patents ${data?.name ? `with ${data?.name} chemical` : ''}`}>
                {(loadingPatents || loading) && <Loader />}
                <Table headers={tableHeaders} rows={docRows} hasActions={true} hasIndex={true} />
            </Card>
        </div>
    )
}

export default DocumentScene
