export type ErrorBoundaryProps = {}

export type ErrorBoundaryState = {
    error: string
    errorInfo: React.ErrorInfo | undefined
    hasError: boolean
}
