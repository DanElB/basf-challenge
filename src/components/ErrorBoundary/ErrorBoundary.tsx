import React, { Component } from 'react'

import { ErrorBoundaryProps, ErrorBoundaryState } from './ErrorBoundary.types'

class ErrorBoundary extends Component<ErrorBoundaryProps, ErrorBoundaryState> {
    state: ErrorBoundaryState = {
        error: '',
        errorInfo: undefined,
        hasError: false,
    }

    // TODO: Implement log service
    logErrorToServices = console.log

    static getDerivedStateFromError(error: Error) {
        return { hasError: true, error }
    }

    componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
        if (process.env.NODE_ENV !== 'development') {
            this.setState({ errorInfo })
        }
        this.logErrorToServices(error.toString(), errorInfo.componentStack)
    }

    render() {
        const { hasError, error } = this.state
        let renderReturn: React.ReactNode = this.props.children
        if (hasError) {
            renderReturn = error
        }

        return renderReturn
    }
}

export default ErrorBoundary
