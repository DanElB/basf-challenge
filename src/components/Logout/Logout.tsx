import React from 'react'

import { User } from '../../types'
import { useUser } from '../../context/User'
import { GOOGLE_CLIENT_ID } from '../../constants'

import styles from './Logout.module.css'

import { GoogleLogout } from 'react-google-login'

const defaultUser: User = {
    name: '',
    email: '',
    image: '',
    isLogged: false,
}

const Logout = ({}) => {
    const userCtx = useUser()

    const handleGoogleLogout = () => {
        userCtx.setUser(defaultUser)
    }

    return (
        <div className={styles.modal}>
            {userCtx.user?.isLogged && (
                <GoogleLogout
                    clientId={GOOGLE_CLIENT_ID}
                    buttonText="Logout"
                    onLogoutSuccess={handleGoogleLogout}
                    render={renderProps => (
                        <small className={styles.button} onClick={renderProps.onClick}>
                            Log out
                        </small>
                    )}
                />
            )}
        </div>
    )
}

export default Logout
