import React from 'react'

import { DrawerProps } from './Drawer.types'

import { useUser } from '../../context/User'

import styles from './Drawer.module.css'
import { ReactComponent as Logo } from '../../assets/logos/BASF_Logo.svg'

import Link from '../Link/Link'
import Login from '../Login/Login'
import Logout from '../Logout/Logout'
import ThemeToggler from '../ThemeToggler/ThemeToggler'

import { Row, Col } from 'react-flexbox-grid'

const Drawer: React.FC<DrawerProps> = () => {
    const userCtx = useUser()

    return (
        <aside className={styles.container}>
            <Logo className={styles.logo} />

            <div className={styles.toggleButton}>
                <ThemeToggler />
            </div>

            <Link to="/search">Search</Link>
            <Link to="/documents">Documents</Link>

            <hr className={styles.linkBottom} />
            <Login />
            <Row middle="xs" className={styles.userInfo}>
                <Col xs>
                    {userCtx?.user?.image && (
                        <div className={styles.image}>
                            <img src={userCtx?.user?.image} alt="User image" />
                        </div>
                    )}
                </Col>
                <Col xs>
                    <p className={styles.name}>{userCtx?.user?.name}</p>
                    <p className={styles.email}>{userCtx?.user?.email}</p>
                    <Logout />
                </Col>
            </Row>

            <footer className={styles.footer}>
                <hr />
                <small className={styles.copyright}>© BASF SE 2020</small>
                <a href="mailto:d.barbero.daniel@gmail.com?subject=BASF%20Challenge%20" target="_blank">
                    Daniel Barbero Muñoz
                </a>
            </footer>
        </aside>
    )
}

export default Drawer
