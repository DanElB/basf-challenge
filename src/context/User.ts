import React, { useContext } from 'react'

import { User } from '../types'

type ContextProps = {
    user?: User
    setUser: Function
}

export const UserContext = React.createContext<ContextProps>({
    user: {
        name: '',
        email: '',
        image: '',
        isLogged: false,
    },
    setUser: () => null,
})

export const useUser = () => useContext(UserContext)
