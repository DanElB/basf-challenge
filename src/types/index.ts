export type User = {
    name: string
    email: string
    image: string
    isLogged: boolean
}

export type RelatedChemical = {
    name: string
    partial: number
}

export type RelatedPatent = {
    patentNo: string
    patentTitle: string
}

export type RelatedChemicals = {
    type1: RelatedChemical[] | null
    type2: RelatedChemical[] | null
}

export type RelatedPatents = {
    type1: RelatedPatent[]
    type2: RelatedPatent[]
}

export type Chemical = {
    name: string
    total: number
    relatedChemicals: RelatedChemicals
    relatedPatents: RelatedPatents
}
