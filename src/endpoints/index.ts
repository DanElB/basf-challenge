import { gql } from '@apollo/client'

export const GET_CHEMICALS = gql`
    query getChemicals($name: String) {
        chemicals(name: $name)
    }
`

export const GET_CHEMICAL = gql`
    query getChemical($name: String!) {
        name(name: $name)
        total(name: $name)
        relatedChemicals(name: $name) {
            type1 {
                name
                partial
            }
            type2 {
                name
                partial
            }
        }
        relatedPatents(name: $name) {
            type1 {
                patentNo
                patentTitle
            }
            type2 {
                patentNo
                patentTitle
            }
        }
    }
`

export const GET_PATENTS = gql`
    query getPatents {
        patents {
            patentNo
            patentTitle
        }
    }
`
