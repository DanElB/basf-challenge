## Installation

To run this project you need to have _Node_ and _npm_ installed in your machine.\
I am using _Node_ `v15.0.1` and _npm_ `6.14.5`. I recommend using those versions to avoid compatibility issues but you can also try older versions.\
If you already have _Node_ and _npm_ installed on your machine, go to step 3.

1. Download and install Node from [https://nodejs.org/en/](https://nodejs.org/en/). You can check that it has been installed correctly by opening a new terminal and doing:

```shell
node -v
# v15.0.1
npm -v
# 6.14.5
```

2. If you want to match my Node version you can use the `npm n` package, otherwise skip this step.\
   Open a new terminal:

```shell
npm install -g n
n 15.0.1
node -v
# v15.0.1
```

Also I am using `yarn` instead of `npm` for package management. You always can use `npm` so this step is not required.
    
```shell
npm install -g yarn
yarn -v
# 1.22.5
```

3. Unzip the repository and move into the folder.

```shell
# Move into the project location
cd ~/path/to/folder/basf-challenge
```

4. Install the project dependencies

```shell
# npm install
yarn install
# Long wait happens here :)
```

5. Run the project, go to [http://localhost:3000](http://localhost:3000) and enjoy it!

```shell
# npm run dev
yarn dev
```

---

### Technologies

For this project I have use these technologies:

-   React to build the UI
-   Typescript as programming language
-   CSS modules for styling
-   Prettier for code formatting
-   React Router for navigation components
-   Apollo Server + GraphQL to build the API
-   Apollo Client to query the API
-   react-google-login to get the google account data via OAuth2

## Things to improve

-   i18n. I would use some localization software like Phrase
-   Responsive
-   API optimization
-   Database use (MongoDB) to store data instead of serving the JSON directly
-   Testing

---

### Available Scripts

In the project directory, you can run:

### `yarn api` or `npm run api`

Runs the Apollo server in the development mode.\
Open [http://localhost:4000](http://localhost:4000) to view it in the browser and play with the GraphQL playground.

### `yarn start` or `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn dev` or `npm run dev`

Runs the Apollo server and the React app in development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in action.
